package com.coursera.cspsaa.assignment.countdown;

import java.lang.Thread.State;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class MainActivity extends Activity {

	private static final long MILLS_PER_SECOND = 1000;
	private static final long MILLS_PER_MINUTE = MILLS_PER_SECOND * 60;
	private static final long MILLS_PER_HOUR = MILLS_PER_MINUTE * 60;
	private static final long MILLS_PER_DAY = MILLS_PER_HOUR * 24;

	private TextView tvYearLeft;
	private TextView tvYearCenter;
	private TextView tvYearRight;

	private TextView tvHoursLeft;
	private TextView tvHoursRight;

	private TextView tvMinutesLeft;
	private TextView tvMinutesRight;

	private TextView tvSecondsLeft;
	private TextView tvSecondsRight;

	private GregorianCalendar dateTimeNow;
	private GregorianCalendar target;

	private long diff;
	private int secondsSpan;
	private int minutesSpan;
	private int hoursSpan;
	private int daysSpan;

	private Handler hndlr;

	private Runnable taskSeed;
	private Runnable postSeed;
	private Thread backgroundThread;
	
	MediaPlayer mediaPlayer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		initialize();
		backgroundThread = new Thread(taskSeed);

	}

	private void initialize() {
		tvYearLeft = (TextView) findViewById(R.id.tvYearLeft);
		tvYearCenter = (TextView) findViewById(R.id.tvYearCenter);
		tvYearRight = (TextView) findViewById(R.id.tvYearRight);
		tvHoursLeft = (TextView) findViewById(R.id.tvHoursLeft);
		tvHoursRight = (TextView) findViewById(R.id.tvHoursRight);
		tvMinutesLeft = (TextView) findViewById(R.id.tvMinutesLeft);
		tvMinutesRight = (TextView) findViewById(R.id.tvMinutesRight);
		tvSecondsLeft = (TextView) findViewById(R.id.tvSecondsLeft);
		tvSecondsRight = (TextView) findViewById(R.id.tvSecondsRight);
		
		mediaPlayer = MediaPlayer.create(this, R.raw.final_music);

		hndlr = new Handler();

		taskSeed = new Runnable() {

			@Override
			public void run() {
				while (!Thread.interrupted()) {
					hndlr.post(postSeed);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						return;
					}
				}
			}
		};
		postSeed = new Runnable() {
			public void run() {
				updateSpan();
			}
		};
	}

	private void updateSpan() {
		dateTimeNow = new GregorianCalendar();
		target = new GregorianCalendar(dateTimeNow.get(Calendar.YEAR) + 1, 0,
				0, 23, 59, 59);
		diff = (target.getTimeInMillis() - dateTimeNow.getTimeInMillis());
		if(diff <= 1000f){
			mediaPlayer.start();
		}
		secondsSpan = getSeconds();
		minutesSpan = getMinutes();
		hoursSpan = getHours();
		daysSpan = getDays();

		setAllControls();
	}

	private int getDays() {
		return (int) (diff / MILLS_PER_DAY);
	}

	private int getHours() {
		return (int) (diff % MILLS_PER_DAY / MILLS_PER_HOUR);
	}

	private int getMinutes() {
		return (int) (diff % MILLS_PER_HOUR / MILLS_PER_MINUTE);
	}

	private int getSeconds() {
		return (int) (diff % MILLS_PER_MINUTE / MILLS_PER_SECOND);
	}

	private void setAllControls() {
		setDaysControl(daysSpan);
		setHoursControl(hoursSpan);
		setMinutesControl(minutesSpan);
		setSecondsControl(secondsSpan);
	}

	private void setHoursControl(int hours) {
		tvHoursLeft.setText(Integer.toString((int) (hours / 10)));
		tvHoursRight.setText(Integer.toString(hours % 10));
	}

	private void setMinutesControl(int minutes) {
		tvMinutesLeft.setText(Integer.toString((int) (minutes / 10)));
		tvMinutesRight.setText(Integer.toString(minutes % 10));
	}

	private void setSecondsControl(int seconds) {
		tvSecondsLeft.setText(Integer.toString((int) (seconds / 10)));
		tvSecondsRight.setText(Integer.toString(seconds % 10));
	}

	private void setDaysControl(int days) {
		tvYearLeft.setText(Integer.toString((int) (days / 100)));
		tvYearCenter.setText(Integer.toString((int) (days % 100 / 10)));
		tvYearRight.setText(Integer.toString(days % 10));
	}

	@Override
	protected void onStart() {
		super.onStart();
		State current = backgroundThread.getState();
		if(current == State.NEW){
		backgroundThread.start();
		} else if(current == State.TERMINATED){
			backgroundThread = new Thread(taskSeed);
			backgroundThread.start();
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		mediaPlayer.stop();
		backgroundThread.interrupt();
	}

}
